---
title: "useR! 2023 summer regional events"
output: 
  flexdashboard::flex_dashboard:
    logo: "palm-tree.png"
    theme:
      bg: "#fff7e0"
      fg: "#ff6f69" 
      primary: "#ff6f69"
      base_font:
        google: Prompt
      code_font:
        google: JetBrains Mono
    orientation: columns
    vertical_layout: fill
---

```{r setup, include=FALSE}
library(flexdashboard)
# Install thematic and un-comment for themed static plots (i.e., ggplot2)
# thematic::thematic_rmd()

library(dplyr)
library(glue)
library(gt)
library(purrr)
library(htmltools)



# Load data -------------------------------
# Events data -----------
events <- readRDS("app_data/events.rds") %>%
  # Edit data 
  # has it happened?
  mutate(
    has_happened = if_else(Sys.Date() > start_date, TRUE, FALSE)
  )

# Abstracts data -----------
abstracts <- readRDS("app_data/abstracts.rds") 

```

 ```{css}
.chart-shim {
  overflow: auto;
}
```

Sidebar {.sidebar}
=====================================

### About

This app aims to help promote events dedicated to the R community 
taking place this summer.

### Supporters

![](rconsort-logo-small.png)

We'd like to thank the R Consortium and R Foundation for 
their support and endorsement.

Events
=====================================  

Column {data-width=650}
-----------------------------------------------------------------------

### Events

```{r, echo=FALSE, results='asis'}

make_title <- function(event_name, type, image){
      glue::glue(
        "<img src='images/{image}' alt='' height='90'/><div style='line-height:10px'><span style='font-weight:bold;font-variant:small-caps;font-size:14px'>{event_name}</div>
        <div style='line-height:12px'><span style ='font-weight:bold;color:grey;font-size:10px'>{type}</span></div>"
      )
}

make_info <- function(location,start_date,website){
     website_simple <- gsub("http://","",website)
     website_simple <- gsub("https://","",website_simple)
     website_simple <- gsub("/","",website_simple)
     website_simple <- gsub("www.","",website_simple)
  
      glue::glue(
        "<div style='line-height:10px'><span style='font-weight:bold;font-variant:small-caps;font-size:14px'>{location}</div>
        </br>
        <div style='line-height:12px'><span style ='font-weight:bold;color:grey;font-size:14px'>Kicks off: {start_date}</span></div>
        </br>
        </br>
        <div style='line-height:10px'><span style='font-weight:bold;font-variant:small-caps;font-size:14px'><a href='{website}'>{website_simple}</a></div>"
      )
    }

table <- events |> 
  mutate(
    Event = make_title(event_name, type, image),
    Event = map(Event, gt::html),
    Info = make_info(location,start_date,website),
    Info = map(Info, gt::html)
  ) |>
  select(
    Event, Description = description, Info
  ) |>
  gt() |>
  opt_interactive(
    use_search = TRUE,use_filters = TRUE, use_pagination = FALSE
  )

table
  
```
